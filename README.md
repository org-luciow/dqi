# dqi

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


TODO

done - Generate link
done - Decode the DQI from URL
done - Prettify the UI
done - Add the page with whole policy rendered out of mark down
done - Rewrite the policy into MD
Add favicon
Add left bar with links 
google analytics
