module.exports = {
    transpileDependencies: [
        'vuetify'
    ], configureWebpack: {
        module: {
            rules: [
                {
                    test: /\.md$/i,
                    use: [
                        {
                            loader: 'raw-loader',
                            options: {
                                esModule: false,
                            },
                        },
                    ],
                },
            ],
        },
    }
}
